package world.ws_ultim;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/echo")
public class chatpls {
	List<String[]> aliases;


	void printSessions(Session session) {
		Set<Session> sessions = session.getOpenSessions();
		System.out.println("Opened sessions: " + sessions + ".\nSession IDs:");
		for (Session s : sessions)
			System.out.print(s.getId() + ", ");
	}

	Pattern patt = Pattern.compile("[\\s+]");

	void handle_cmd(Session session, String message){
	String s_id = session.getId();
	int ml = message.length();
	if(message.startsWith("#set-username ", 0)) {
		if(ml > 14) {
			String tobeusername = message.substring(14);
			String[] uname = null;
			try{
				uname = message.split(" ", 2);
			} catch (Exception ex) {}
			if (uname != null) {
				Matcher mat = patt.matcher(tobeusername);
				if (mat.find()) {
					String errmsg = "ERROR> @" + s_id + " whitespace is not allowed in usernames, thus " + tobeusername + "won't do";
					System.err.println(errmsg);
					try {
						session.getBasicRemote().sendText(errmsg);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					String old_clid = get_username(session);
					s_id = tobeusername;
					set_username(tobeusername, session);
					String infomsg = "INFO> " + old_clid + " is from now on known as " + s_id;
					System.err.println(infomsg);
					try {
						session.getBasicRemote().sendText(infomsg);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	} else if(message.startsWith("#dm ", 0)) {
		String[] tbuname = message.split(" ", 3);
		if (tbuname.length < 3) {
			String errmsg = "ERROR> @" + s_id + " you have to provide a clientID of the addressee";
			System.err.println(errmsg);
			try {
				session.getBasicRemote().sendText(errmsg);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (get_username(session).equals(get_username(find_username(tbuname[1], session)))) {
			String errmsg = "ERROR> @" + s_id + " cannot send DMs to yourself..";
			System.err.println(errmsg);
			try {
				session.getBasicRemote().sendText(errmsg);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if ("".equals(get_username(find_username(tbuname[1], session)))) {
			String errmsg = "ERROR> @" + s_id + " unknown username \"" + tbuname[1] + "\"...";
			System.err.println(errmsg);
			try {
				session.getBasicRemote().sendText(errmsg);
			} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				String msg = "Dm from "+s_id + " to @" + tbuname[1] + " > " + tbuname[2];
				senddm (msg, tbuname[1], session);
			}
		} else {
			sendMessageToAll(message, session);
		}
	}

	void sendMessageToAll(String message, Session session) {
		Set<Session> sessions = session.getOpenSessions();
		for (Session s : sessions)
			if (s != session)
				try {
					s.getBasicRemote().sendText(message);
				} catch (IOException e) {
					e.printStackTrace();
				}
	}

	void senddm(String message, String whoto, Session session) {
		Set<Session> sessions = session.getOpenSessions();
		for (Session s : sessions){
			if (s != session){
				try {
					String uname = s.getUserProperties().get("username").toString();
					if(uname.equals(whoto))
					{
						System.out.println("INFO> Sending a dm");
						s.getBasicRemote().sendText(message);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	void set_username(String username, Session session) {
		session.getUserProperties().put("username", username);
		String info = "Username for " + session.getId() + " changed to:" + username;
		sendMessageToAll(info, session);
	}

	Session find_username(String username, Session session) {
		Session ret_session = null;

		Set<Session> sessions = session.getOpenSessions();
		for (Session s : sessions) {
			String uname = null;
			try {
				String sa = s.getUserProperties().get("username").toString();
				uname = sa.substring(1);
				System.err.println(uname);
			} catch (Exception e) {
				System.err.println(e);
			}
			if (uname == null) {
				uname = s.getId().toString();
			}

			if (uname.equals(username)) {
				ret_session = s;
				break;
			}
		}

		return ret_session;
	}

	String get_username(Session session) {
		String uname = null;
		if (session == null) {
			return uname = "";
		}
		try {
			String sa = session.getUserProperties().get("username").toString();
			uname = sa.substring(1);
			System.err.println("getusername: custom username - " + uname);
		} catch (Exception e) {
			System.err.println("no custom username " + e);
		}
		try{
			if (uname == null) {
				uname = session.getId().toString();
				System.err.println("getusername: default username - " + uname);
			}
		} catch (Exception e){
			System.err.println(e);
		}
		return uname;
	}

	void reset_username(String message, Session session) {
		set_username(session.getId(), session);
	}

	@OnOpen
	public void onOpen(Session session) {
		System.out.println("Open Connection ...");
		printSessions(session);
	}

	@OnClose
	public void onClose(Session session) {
		System.out.println("Close Connection ..." + session.getId());
		printSessions(session);
	}

	@OnMessage
	public void onMessage(String message, Session session) {
		printSessions(session);
		System.out.println("Message from the client id: " + session.getId() + ", uname: " + get_username(session) + " = " + message);
		handle_cmd(session, message);
		return;
	}

	@OnError
	public void onError(Throwable e) {
		e.printStackTrace();
	}

}
